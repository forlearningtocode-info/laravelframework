<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $casts = [
        'params' => 'collection',
        'data' => 'collection',
    ];

    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function getName()
    {
        return "WEB-" . $this->id;
    }

    public function getCurrencyText()
    {
        return "mdl";
    }

    public function getCartItems()
    {
        $common = json_decode($this->data, true);
        return $common;
    }

    public function getSubTotalAmount()
    {
        $total_amount = 0;
        foreach ($this->getCartItems() as $item) {
            $total_amount += $item['price'] * $item['quantity'];
        }
        return $total_amount;
    }

    public function getOrderAmount()
    {
        $total_amount = $this->getSubTotalAmount();
        $total_amount += $this->getShipping();
        return $total_amount;
    }

    public function getShipping()
    {
        return 0;
    }

    public function getSale()
    {
        return 0;
    }

    public function getDate()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->created_at)->format('d/m/Y');
    }

    public function getTotalQuantity(){
        $total_quantity = 0;
        foreach ($this->getCartItems() as $item) {
            $total_quantity += $item['quantity'];
        }
        return $total_quantity;
    }

    public function getProductIds(){
        $product_ids = array();

        foreach ($this->getCartItems() as $item) {
            $product_ids[] = $item['product_id'];
        }

        return $product_ids;
    }

}
