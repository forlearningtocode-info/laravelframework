<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class Photos extends Model
{
    protected $fillable = ['table_id', 'table', 'source'];
    public $timestamps  =   false;

    public static function addWatermark($inputFile, $outputFile = '')
    {
        //check if file exists
        if (!File::exists($inputFile)) {
            return false;
        }

        if ($outputFile == '') {
            $outputFile = $inputFile;
        }

        //get from custom configuration file
        $watermarkFile = base_path() . "/" . config('custom.watermark');

        // open an image file
        $img = Image::make($inputFile);

        //init watermark source
        $watermark = Image::make($watermarkFile);

        //if watermark width is bigger than half of image width
        if ($watermark->width() > $img->width() / 2) {
            // resize only the width of the watermark
            $watermark->resize($img->width() / 2, null, function ($constraint) {
                $constraint->aspectRatio();  //proportional
            });
        }

        // insert a watermark
        $img->insert($watermark, 'bottom-right');

        // finally we save the image as a new file
        $img->save($outputFile);
    }
}
