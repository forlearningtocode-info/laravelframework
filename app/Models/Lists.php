<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class Lists extends BaseModel
{
    public $timestamps  =   false;

    protected $with = [
        'photos',
        'children'
    ];

    public function children()
    {
        return $this->hasMany('App\Models\Lists', 'parent_id')->orderBy('sort');
    }

    public function getValueAttribute()
    {
        return $this->attributes['description_short'];
    }

    //search by slug
    public static function get($slug)
    {
        $list = self::where('slug', $slug)->with(['children' => function($query){
            $query->with('photos')->enabled();
        }])->first();
        if ( ! isset($list) ) {
            return new Collection();
        }
        return $list->children;
    }

    public function getParamsObj()
    {
        $params = unserialize($this->attributes['params']);

        $data = [];

        if ($params) {
            foreach ($params as $param) {
                if (!isset($param['key'])) continue;

                $data[$param['key']] = $param['value'];
            }
            return (object)$data;
        }

        return $data;
    }

    public function getReserve($type = "")
    {
        $arr = explode(",", $this->attributes['reserve']);
        if(is_array($arr)){
            if($type == "products"){
                return Product::whereIn('id', $arr)->with('category', 'photos')->get();
            }
        }

        return $this->attributes['reserve'];
    }

    public function getBtnName()
    {
        $locale = Lang::locale();

        if ($locale != config('app.base_locale') && isset($this->getParamsObj()->{'btn_name_'.$locale})) {
            return $this->getParamsObj()->{'btn_name_'.$locale};
        }

        return $this->getParamsObj()->btn_name_ru;
    }

    public function getBtnLink()
    {
        $locale = Lang::locale();

        if ($locale != config('app.base_locale') && isset($this->getParamsObj()->{'btn_link_'.$locale})) {
            return $this->getParamsObj()->{'btn_link_'.$locale};
        }

        return $this->getParamsObj()->btn_link_ru;
    }
}
