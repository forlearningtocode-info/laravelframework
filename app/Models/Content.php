<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;

class Content extends BaseModel
{
    protected   $table      = "content";

    public static function get($slug, $vars = [])
    {
        //search by slug
        $content = self::where('slug', $slug)->first();
        if (!isset($content)) {
            return '';
        }

        $text = $content->description;
        $keys = array_keys($vars);
        $values = array_values($vars);
        foreach($keys as $k => $v) {
            $keys[$k] = '%' . $v . '%';
        }
        return str_replace($keys, $values, $text);
    }
}
