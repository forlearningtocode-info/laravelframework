<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $casts = [
        'params' => 'collection',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'open_password', 'phone', 'address', 'params'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'open_password',
    ];

    public function isManager()
    {
        // this looks if is manager in rights column in your users table
        if ($this->rights == 2) return true;
        return false;
    }

    public function isAdmin()
    {
        // this looks if is admin in rights column in your users table
        if ($this->rights == 1) return true;
        return false;
    }

    public function getCakePhpPassword()
    {
        return $this->open_password;
    }

}
