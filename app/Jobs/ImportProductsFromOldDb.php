<?php

namespace App\Jobs;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class ImportProductsFromOldDb implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = DB::connection('superoffice_old')
                    ->table('products')
                    ->where('title_rus', '!=', '')
                    ->limit(10)
                    ->get();



        foreach ($data as $item) {

            dump($item);

            $product = new Product();
            $product->category_id = $item->category_id; // временно
            $product->product_number = $item->partnumber;
            $product->name = $item->title_rus;
            $product->name_ro = $item->title_rom;
            $product->description = $item->content_rus;
            $product->description_ro = $item->content_rom;
            $product->characteristics = $item->tech_rus;
            $product->characteristics_ro = $item->tech_rom;

            // $params['video'] = $item->video;
            // $params['proposal_id'] = $item->proposal;
            // $params['proposal_title_ru'] = '';
            // $params['proposal_title_ro'] = '';
            $params['proposal_text_ru'] = $item->proposaltext_rus;
            $params['proposal_text_ro'] = $item->proposaltext_rom;
            $params['color'] = $item->color;
            // $params['gift_id'] = '';
            $product->params = $params;

            $product->price = $item->price;
            $product->price_today = $item->dayprice;
            $product->sale = $item->discount;
            $product->sale_vip = $item->pdisc;
            $product->sale_gold = $item->gdisc;
            $product->sale_silver = $item->udisc;

            $product->slug = $item->alias;
            $product->enabled = $item->published;

            $product->type = $item->type;
            $product->top_mainpage = $item->productofday;
            $product->top_category = $item->productofdaycat;
            $product->recommended = $item->recommand;
            $product->bestseller = $item->bestseller;
            $product->just_purchased = $item->sold;
            $product->navigation_menu = $item->productmenu;
            $product->has_gift = $item->is_gift;
            $product->availability = $item->quantity;
            $product->is_virtual = $item->is_root;

            $product->reserve = $item->id;

            dd($product);
        }


    }
}
