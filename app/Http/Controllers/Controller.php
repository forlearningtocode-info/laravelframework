<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use DataTables;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Controller constructor.
     */
    public function __construct()
    {

    }

    public function UpdatePhotos(Request $request, $id)
    {
        $pc = new Admin\PhotosController;
        $pc->UpdatePhotos($request, $id);
    }

    public static function datatablesCommon($query)
    {
        return Datatables::eloquent($query)
                ->editColumn('name', function ($item) {
                    return view('admin.partials.datatable-title', compact('item'))->render();
                })
                ->addColumn('actions', function ($item) {
                    return view('admin.partials.datatable-actions', compact('item'))->render();
                })
                ->rawColumns(['name', 'actions']);
    }

}
