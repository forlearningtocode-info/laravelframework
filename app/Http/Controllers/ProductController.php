<?php

namespace App\Http\Controllers;

use App\Models\Categories;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getProduct($product_slug)
    {
        $product = Product::enabled()
            ->where('slug', $product_slug)
            ->firstOrFail();

        // добавляем просмотров
        $product->increment('views');

        return view('product')->with(compact('product'));
    }

    public function getProductsCategory($category_slug)
    {
        $category = Categories::where('slug', $category_slug)->first();

        $products = Product::enabled()->whereHas('category', function ($query) use ($category_slug) {
            $query->where('slug', $category_slug);
        })->get();

        return view('products')->with(compact('category', 'products'));
    }

    public function getCategories()
    {
        $categories = Categories::enabled()->get();

        return view('categories')->with(compact('categories'));
    }
}
