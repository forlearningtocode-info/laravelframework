<?php

namespace App\Http\Controllers;

use App\Models\Content;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    public function getBySlug($slug)
    {
        $data = Content::where('slug', $slug)->firstOrFail();

        return view('content')->with(compact('data'));
    }

    public function getSelect()
    {
        return view('select');
    }

}
