<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Content;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Lists;
use Illuminate\Support\Facades\Mail;


class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['getCart', 'getCheckout']);
    }

    /**
     * Show the application index page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $background = Lists::where('id', 1)->first();
        $work = Lists::where('id', 2)->first();
        $block_video = Lists::where('id', 3)->first();
        $projects = Product::enabled()->get();
        $apartments = Lists::where('id', 4)->first();

        return view('index')->with(compact('background', 'work', 'block_video', 'projects', 'apartments'));
    }

    public function changeItemsPerPage(Request $request)
    {
        session(['per_page' => $request->per_page]);
    }


    public function sendContactForm()
    {
        // Sending email to admin
        Mail::send('emails.contact_form', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->from(env('MAIL_USERNAME'), "City Gardens")
                ->subject('Новая заявка с сайта City Gardens');
        });

        if (Mail::failures()) {
            return response()->Fail('Ошибка отправки сообщения!');
        }

        return ["success" => true];
    }

    public function sendConsultationForm()
    {
        // Sending email to admin
        Mail::send('emails.consultation_form', ['data' => request()], function ($message) {
            $message->to(config('custom.admin_email'))
                ->from(env('MAIL_USERNAME'), "City Gardens")
                ->subject('Новая заявка на консультацию с сайта City Gardens');
        });

        if (Mail::failures()) {
            return response()->Fail('Ошибка отправки сообщения!');
        }

        return ["success" => true];
    }
}

