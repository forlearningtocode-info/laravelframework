<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Partner;
use App\Models\Products;
use App\Models\Vendor;
use Illuminate\Http\Request;
use App\Models\User;

use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    public function index()
    {
        return view('admin.users.index');
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxData()
    {
        $query = User::query();

        return self::datatablesCommon($query)
            ->make(true);
    }

    public function create()
    {
        return view('admin.users.edit')->with($this->common());
    }

    public function store(Request $request)
    {
        $rules = array(
            'name'          => 'required',
            'email'         => 'required',
        );

        $this->validate($request, $rules);

        return $this->save($request, null);
    }

    public function edit($id)
    {
        $data['data'] = User::find($id);
        return view('admin.users.edit', $data)->with($this->common());
    }

    public function save(Request $request, $id)
    {

        if (!isset($id)) {
            $data = new User();
        } else {
            $data = User::find($id);
        }

        $params = [
                'company' => $request->company,
                'fisc' => $request->fisc,
                'bank' => $request->bank,
                'bank_account' => $request->bank_account,
                'tva' => $request->tva,
                'fax' => $request->fax,
        ];

        $data->params = $params;

        $data->name                 = $request->name;
        $data->email                = $request->email;
        $data->phone                = $request->phone;
        $data->rights               = $request->rights;
        $data->enabled              = $request->enabled;
        $data->address              = $request->address;

        if ($request->password != '') {
            $data->password = bcrypt($request->password);
        }
        $data->save();

        // redirect
        Session::flash('message', trans('common.saved'));
        return redirect('admin/users');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name'           => 'required',
            'email'          => 'required',
        );

        $this->validate($request, $rules);

        return $this->save($request, $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        User::destroy($id);
        Session::flash('message', trans('common.deleted'));
        return back();
    }

    public function common()
    {
        return [];
    }
}