<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Categories;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogController extends Controller
{
    public function getParentCategory(Request $request)
    {
        $category = Categories::where('slug', $request->parent_category)->firstOrFail();

        $category_products = Categories::has('products')->whereHas('parents', function ($query) use ($category) {
            $query->where('parent_id', $category->id);
        })->with('parents')->with('products')->with('products.photos')->get();

        return view('category')
            ->with(compact('category', 'category_products'));
    }

    public function getChildrenCategory(Request $request)
    {
        $category_children = Categories::where('slug', $request->children_category)->firstOrFail();

        $productsQuery = Product::enabled()->whereHas('category', function ($query) use ($request) {
            $query->where('slug', $request->children_category);
        });

        $brands = Brand::where('enabled', true)->get();

        // filter data
        $filter = [];

        if ($request->has('brands')) {
            $productsQuery->whereIn('brand_id', $request->brands);
            $filter['brands'] = $request->brands;
        }

        if ($request->available != 'No') {
            // доступность на складе
            $productsQuery->whereIn('availability', [1, 2]);
            $filter['available'] = true;
        }

        if ($request->has('filter')) {
            $productIds = [];
            foreach ($request->filter as $parameter_id => $value) {
                if (is_array($value)) { // если массив значений чекбоксов
                    $ids = DB::table('parameters_products')
                        ->where('parameter_id', $parameter_id)
                        ->whereIn('value_id', $value)
                        ->get()
                        ->pluck('product_id')
                        ->toArray();
                    $filter[$parameter_id] = $value;
                } else { // интервал
                    $pieces = explode(';', $value);
                    $from = isset($pieces[0]) ? (int) $pieces[0] : 0;
                    $to = isset($pieces[1]) ? (int) $pieces[1] : 0;
                    $ids = DB::table('parameters_products')
                        ->where('parameter_id', $parameter_id)
                        ->whereBetween('value', [$from, $to])
                        ->get()
                        ->pluck('product_id')
                        ->toArray();
                    $filter[$parameter_id]['from'] = $from;
                    $filter[$parameter_id]['to'] = $to;
                }

                if (count($productIds) == 0) {
                    $productIds = $ids;
                } else {
                    // пересечение отфильтрованных ID
                    $productIds = array_intersect($productIds, $ids);
                }
            }

            $productsQuery->whereIn('id', $productIds);
        }


        // узнаем максимальную цену (должно быть до фильтра с интервалом цены)
        $maxPrice = (int) round($productsQuery->max('price'));
        $minPrice = (int) round($productsQuery->min('price'));
        if ($maxPrice == 0) {
            // максимальная по всем товарам
            $maxPrice = (int) round(\App\Models\Product::max('price'), -3);
        }

        if ($request->has('price_range')) {
            $values = explode(';', $request->price_range);
            $priceFrom = isset($values[0]) ? $values[0] : 0;
            $priceTo = isset($values[1]) ? $values[1] : 0;
            $productsQuery->whereBetween('price', [$priceFrom, $priceTo]);
            $filter['price_from'] = $priceFrom;
            $filter['price_to'] = $priceTo;
        }

        $productsIds = $productsQuery->get(['id'])->pluck('id')->toArray();

        $products = $productsQuery->paginate(session('per_page', 8));

        $lastIdx = $request->get('lastIdx', 0);

        return view('category2')
            ->with(compact('category_children', 'products', 'brands', 'filter', 'minPrice', 'maxPrice', 'productsIds', 'lastIdx'));
    }

    public function getRecommendedProducts(){
        $recommended_products = Product::enabled()->where('recommended', true)->whereIn('availability', [1,2])->paginate(session('per_page', 8));

        return view('recommended_products')->with(compact('recommended_products'));
    }

    public function getBestsellerProducts(){
        $bestseller_products = Product::enabled()->where('bestseller', true)->whereIn('availability', [1,2])->paginate(session('per_page', 8));

        return view('bestseller_products')->with(compact('bestseller_products'));
    }

    public function getNewProducts(){
        $new_products = Product::enabled()->where('new', true)->whereIn('availability', [1,2])->paginate(session('per_page', 8));

        return view('new_products')->with(compact('new_products'));
    }

    public function getDiscountProducts(){
        $products = Product::enabled()
            ->available()
            ->where('has_discount', true)
            ->paginate(session('per_page', 8));

        return view('discount_products')->with(compact('products'));
    }
}
