<?php

namespace App\Http\Controllers;

use App\Models\User;

use Illuminate\Auth\Access\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    public function index(){
        $user = User::find(Auth::user()->id);

        $profilePage = true;

        return view('profile')->with(compact('profilePage', 'user'));
    }

    public function update(Request $request)
    {
        $rules = array(
                'company' => 'required',
                'fisc' => 'required',
                'bank' => 'required',
                'bank_account' => 'required',
                'name' => 'required',
                'phone' => 'required',
                'address' => 'required'
        );

        $this->validate($request, $rules);

        return $this->save($request);
    }

    private function save(Request $request){
        $user = User::find(Auth::user()->id);

        $params = [
                'company' => $request->company,
                'fisc' => $request->fisc,
                'bank' => $request->bank,
                'bank_account' => $request->bank_account,
                'tva' => $request->tva,
                'fax' => $request->fax,
        ];

        $user->name = $request->name;
        $user->address = $request->address;
        $user->phone = $request->phone;

        $user->params = $params;

        $user->save();

        Session::flash('message', trans('common.saved'));
        return redirect(route('get-profile'));
    }

    public function changePassword(Request $request){
        $user = User::find(Auth::user()->id);
        $user_password = $user->password;

        $old_password = $request->old_password;
        $new_password = $request->new_password;

        if (Hash::check($old_password, $user_password)) {
            $user->password = bcrypt($new_password);
            $user->open_password = $new_password;
            $user->save();

            return response()->json(["success" => true]);
        }

        return response()->json(["success" => false]);
    }
}