<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\PrinterType::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});
