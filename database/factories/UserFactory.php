<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    static $password;
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'description_short' => $faker->text(50),
        'description' => $faker->text(),
        'remember_token' => str_random(10),
        'city_id' => function () {
            $c = factory(App\Models\City::class)->create();
            return $c->id;
        },
        'cost_per_hour' => $faker->randomFloat(),
        'is_contract_price' => $faker->randomElement([true, false]),
        'balance' => $faker->randomFloat(),
        'phone' => $faker->phoneNumber,
        'birthdate' => $faker->date(),
        'rating' => $faker->randomNumber(2),
        'verified' =>  $faker->randomElement([true, false]),
        'is_customer' =>  $faker->randomElement([true, false]),
        'is_executor' =>  $faker->randomElement([true, false]),
    ];
});
