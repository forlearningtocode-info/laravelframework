<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrintersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('printers', function (Blueprint $t) {
            $t->increments('id');
            $t->integer('printer_category_id');
            $t->integer('printer_type_id');
            $t->string('name');
            $t->string('name_ro');
            $t->string('name_en');
            $t->text('description');
            $t->text('description_ro');
            $t->text('description_en');
            $t->boolean('enabled')->default(true);
            $t->integer('sort');
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('printers');
    }
}
