<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $t) {
            $t->increments('id');
            $t->integer('category_id')->index();
            $t->integer('product_code')->index();
            $t->string('product_number');
            $t->integer('brand_id')->index();
            $t->boolean('new');
            $t->boolean('top_sale');
            $t->string('name');
            $t->string('name_ro');
            $t->string('name_en');
            $t->text('description');
            $t->text('description_ro');
            $t->text('description_en');
            $t->text('characteristics');
            $t->text('characteristics_ro');
            $t->text('characteristics_en');
            $t->text('params');
            $t->float('price', 15, 2);
            $t->float('price_today', 15, 2);
            $t->float('sale', 15, 2);
            $t->float('sale_vip', 15, 2);
            $t->float('sale_gold', 15, 2);
            $t->float('sale_silver', 15, 2);
            $t->string('slug')->unique()->nullable();
            $t->string('slug_ro')->unique()->nullable();
            $t->string('slug_en')->unique()->nullable();
            $t->boolean('enabled')->default(true);
            $t->tinyInteger('type');
            $t->boolean('top_mainpage');
            $t->boolean('top_category');
            $t->boolean('recommended');
            $t->boolean('bestseller');
            $t->boolean('just_purchased');
            $t->boolean('navigation_menu');
            $t->boolean('has_gift');
            $t->tinyInteger('availability');
            $t->boolean('is_virtual');
            $t->integer('views');
            $t->string('reserve', 20);
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
