<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParametersValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parameters_values', function(Blueprint $table) {
            $table->increments('id');
            $table->string('value');
            $table->string('value_ro');
            $table->string('value_en');
            $table->integer('sort');
            $table->integer('parameter_id')->unsigned();
        });

        Schema::table('parameters_values', function(Blueprint $table) {
            $table->foreign('parameter_id')->references('id')->on('parameters')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('parameters_values', function(Blueprint $table) {
            $table->dropForeign('parameters_values_parameter_id_foreign');
        });
        
        Schema::drop('parameters_values');
    }
}
