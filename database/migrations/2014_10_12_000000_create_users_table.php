<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('open_password')->nullable();
            $table->text('description');
            $table->string('phone', 20);
            $table->string('address');
            $table->integer('manager_id');
            $table->tinyInteger('sale_type');
            $table->text('params')->nullable();
            $table->string('discount_card_number');
            $table->tinyInteger('rights')->default(0);
            $table->boolean('enabled')->default(true);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
