<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ListsSeeder::class);
        $this->call(XsortUserSeeder::class);
        $this->call(ProductsSeeder::class);
        $this->call(CartridgesSeeder::class);
        $this->call(PrintersSeeder::class);
        $this->call(SubscribersSeeder::class);
    }
}

class ListsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Lists::class, 100)->create();
    }
}
