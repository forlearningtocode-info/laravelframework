<?php

use Illuminate\Database\Seeder;

class XsortUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = DB::table('users')->where('email', 'support@xsort.md')->count();

        if ($count > 0)
            return;

        DB::table('users')->insert([
            'name'           => "Xsort Web Studio",
            'email'          => "support@xsort.md",
            'password'       => '$2y$10$AOAvbXAn3gEVP6xIBSVTa.Qj2GYc5E9x1iZuZrdMeSrJIS5Mm/34y',
            'rights'         => 1,
        ]);


    }
}
