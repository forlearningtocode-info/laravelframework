<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');

Artisan::command('import-products', function () {
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\ImportProducts());
});

Artisan::command('import-qty-and-prices', function () {
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\ImportQtyAndPrices());
});

Artisan::command('migrate-users', function () {
    app('Illuminate\Bus\Dispatcher')->dispatch(new \App\Jobs\MigrateUsers());
});
