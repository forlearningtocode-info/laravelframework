@extends('admin.body')
@section('title', 'Параметры')
@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ URL::to('admin') }}">Главная</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование</small> </h1>
    </div>

    @include('admin.partials.errors')

    {{ Form::open(['url' => 'admin/configs', 'method' => 'post', 'class' => 'form-horizontal']) }}


    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Бесплатная доставка от (лей) </label>
                <div class="col-sm-9">
                    <input type="text" name="data[constants.free_shipping_amount]" value="{{ config('constants.free_shipping_amount') }}" class="col-xs-10 col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Скидка, % </label>
                <div class="col-sm-9">
                    <input type="text" name="data[constants.sale]" value="{{ config('constants.sale') }}" class="col-xs-10 col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Курс к евро, леи </label>
                <div class="col-sm-9">
                    <input type="text" name="data[constants.exchange_rate]" value="{{ config('constants.exchange_rate') }}" class="col-xs-10 col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Коэффициент прибыли, %</label>
                <div class="col-sm-9">
                    <input type="text" name="data[constants.coefficient]" value="{{ config('constants.coefficient') }}" class="col-xs-10 col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Телефон </label>
                <div class="col-sm-9">
                    <input type="text" name="data[constants.phone]" value="{{ config('constants.phone') }}" class="col-xs-10 col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label no-padding-right"> Email </label>
                <div class="col-sm-9">
                    <input type="text" name="data[constants.email]" value="{{ config('constants.email') }}" class="col-xs-10 col-sm-5">
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection
