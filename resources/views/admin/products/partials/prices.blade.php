<div class="tab-pane" id="prices">

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('price', 'Цена', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('price', (isset($data->price) ? $data->price : old('price')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('price_today', 'Цена сегодня', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('price_today', (isset($data->price_today) ? $data->price_today : old('price_today')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                <label for="sale" class="col-sm-3 control-label no-padding-right">Скидка <small>(Безусловная)</small></label>
                <div class="col-sm-9">
                    {{ Form::text('sale', (isset($data->sale) ? $data->sale : old('sale')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                <label for="sale_vip" class="col-sm-3 control-label no-padding-right">Скидка <small>(V.I.P.)</small></label>
                <div class="col-sm-9">
                    {{ Form::text('sale_vip', (isset($data->sale_vip) ? $data->sale_vip : old('sale_vip')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                <label for="sale_gold" class="col-sm-3 control-label no-padding-right">Скидка <small>(Золото)</small></label>
                <div class="col-sm-9">
                    {{ Form::text('sale_gold', (isset($data->sale_gold) ? $data->sale_gold : old('sale_gold')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                <label for="sale_silver" class="col-sm-3 control-label no-padding-right">Скидка <small>(Серебро)</small></label>
                <div class="col-sm-9">
                    {{ Form::text('sale_silver', (isset($data->sale_silver) ? $data->sale_silver : old('sale_silver')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
        </div>
    </div>

</div>