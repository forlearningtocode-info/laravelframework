<div class="tab-pane" id="description">

    <div class="tabbable tabs-left">

        <ul id="myTab" class="nav nav-tabs">
            <li class="active">
                <a href="#descRu" data-toggle="tab">Описание</a>
            </li>
            <li>
                <a href="#descRo" data-toggle="tab" class="lang-ro">Описание RO</a>
            </li>
            <li>
                <a href="#descEn" data-toggle="tab" class="lang-en">Описание EN</a>
            </li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane in active" id="descRu">
                {{ Form::textarea('description[ru]', (isset($data->description) ? $data->description : old('description')), array('class' => 'ckeditor', 'id' => 'editor')) }}
            </div>
            <div class="tab-pane" id="descRo">
                {{ Form::textarea('description[ro]', (isset($data->description_ro) ? $data->description_ro : old('description_ro')), array('class' => 'ckeditor', 'id' => 'editor_ro')) }}
            </div>
            <div class="tab-pane" id="descEn">
                {{ Form::textarea('description[en]', (isset($data->description_en) ? $data->description_en : old('description_en')), array('class' => 'ckeditor', 'id' => 'editor_en')) }}
            </div>
        </div>

    </div>
</div>