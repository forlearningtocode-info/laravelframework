<div class="tab-pane" id="additional">

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('params[video]', 'Видео', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[video]', (isset($data->params['video']) ? $data->params['video'] : old('params[video]')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('type', 'Тип', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('type', config('custom.product_types'), (isset($data->type) ? $data->type : 0), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[proposal_id]', 'ID товара в предложении', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[proposal_id]', (isset($data->params['proposal_id']) ? $data->params['proposal_id'] : old('params[proposal_id]')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[proposal_title_ru]', 'Заголовок предложения RU', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[proposal_title_ru]', (isset($data->params['proposal_title_ru']) ? $data->params['proposal_title_ru'] : old('params[proposal_title_ru]')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[proposal_title_ro]', 'Заголовок предложения RO', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[proposal_title_ro]', (isset($data->params['proposal_title_ro']) ? $data->params['proposal_title_ro'] : old('params[proposal_title_ro]')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[proposal_text_ru]', 'Текст предложения RU', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[proposal_text_ru]', (isset($data->params['proposal_text_ru']) ? $data->params['proposal_text_ru'] : old('params[proposal_text_ru]')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[proposal_text_ro]', 'Текст предложения RO', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[proposal_text_ro]', (isset($data->params['proposal_text_ro']) ? $data->params['proposal_text_ro'] : old('params[proposal_text_ro]')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('params[color]', 'Выберите цвет', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[color]', (isset($data->params['color']) ? $data->params['color'] : old('params[color]')), ['class' => 'col-sm-11 col-xs-12', 'id' => 'colorpicker']) }}
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            {{--
            <div class="form-group">
                <label class="col-sm-6"> Товар дня <small>(главная страница)</small>:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="top_mainpage"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->top_mainpage) && $data->top_mainpage == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>
            --}}

            {{--
            <div class="form-group">
                <label class="col-sm-6"> Товар дня <small>(категория)</small>:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="top_category"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->top_category) && $data->top_category == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>
            --}}

            <div class="form-group">
                <label class="col-sm-6"> Рекомендуемые товары <small>(блок на главной странице)</small>:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="recommended"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->recommended) && $data->recommended == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6"> Бестселлеры <small>(блок на главной странице)</small>:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="bestseller"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->bestseller) && $data->bestseller == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-6"> Новинка <small></small>:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="new"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->new) && $data->new == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-6"> Top Sale <small></small>:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="top_sale"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->top_sale) && $data->top_sale == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>

            {{--
            <div class="form-group">
                <label class="col-sm-6"> Только что купили <small>(блок на главной странице)</small>:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="just_purchased"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->just_purchased) && $data->just_purchased == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>
            --}}
            {{--
            <div class="form-group">
                <label class="col-sm-6"> Вывод в меню навигации:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="navigation_menu"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->navigation_menu) && $data->navigation_menu == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>
            --}}

            {{--
            <div class="form-group">
                <label class="col-sm-6"> Есть подарок:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="has_gift"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->has_gift) && $data->has_gift == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>

            <div class="form-group @if (isset($data->has_gift) && $data->has_gift == 0) hide @endif" id="gift_form_group">
                {{ Form::label('params[gift_id]', 'ID подарка', ['class' => 'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('params[gift_id]', (isset($data->params['gift_id']) ? $data->params['gift_id'] : old('params[gift_id]')), ['class' => 'col-sm-11 col-xs-12']) }}
                </div>
            </div>
            --}}
            {{--
            <div class="form-group">
                <label class="col-sm-6"> Корневой/Виртуальный товар:</label>
                <div class="col-sm-6">
                    <label>
                        <input name="is_virtual"
                               class="ace ace-switch ace-switch-5"
                               type="checkbox"
                               @if (isset($data->is_virtual) && $data->is_virtual == 1) checked="checked" @endif>
                        <span class="lbl"></span>
                    </label>
                </div>
            </div>
            --}}
        </div>

    </div>

</div>

@section('styles')
    {!! HTML::style('ace/dist/css/colorpicker.min.css') !!}
@append

@section('scripts')

    {!! HTML::script('ace/dist/js/bootstrap-colorpicker.min.js') !!}

    <script>
        $(document).ready(function() {

            $('#colorpicker').colorpicker();

        });
    </script>
@append