@extends('admin.body')
@section('title', 'Товар')


@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ URL::to('admin/products') }}">Объекты</a> <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование объекта </small> </h1>
    </div>

    @if(!isset($data))
    {{ Form::open(['url' => 'admin/products', 'class' => 'form-horizontal']) }}
    @else
    {{ Form::open(['url' => 'admin/products/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <span class="label label-xlg label-light">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </span>
                            <span class="label label-xlg label-light">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                 Изменен: {{ $data->updated_at }}
                            </span>
                            <div class="space-0"></div>
                            <span class="label label-xlg label-light">
                                 Просмотров: {{ $data->views }}
                            </span>
                            @if (isset($data->category->parents{0}))
                                <span class="label label-xlg label-light">
                                    <a href="{{ route('get-product', [$data->category->parents{0}->slug, $data->category->slug, $data->slug]) }}" target="_blank">Посмотреть на сайте</a>
                                </span>
                            @endif
                        @endif
                    </div>
                </div>

            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label class="col-sm-6 "><span class="label label-xlg label-light arrowed-right">Опубликован</span></label>
                    <div class="col-sm-6">
                        <label>
                            <input name="enabled"
                                   class="ace ace-switch ace-switch-5"
                                   type="checkbox"
                                   @if (!isset($data->enabled) || (isset($data) && $data->enabled == 1)) checked="checked" @endif>
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name[ru]', 'Заголовок (RU)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ru]', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12 name_ru')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('name[ro]', 'Заголовок (RO)', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name[ro]', (isset($data->name_ro) ? $data->name_ro : old('name_ro')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->

        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('slug[ru]', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug[ru]', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>
    <div class="space"></div>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>
        </ul>

        <div class="tab-content">
            @include('admin.partials.photos', ['table' => 'products', 'table_id' => isset($data->id) ? $data->id : 0, 'class' => 'showTip L8 active'])
        </div>
</div>

<div class="form-actions">
{{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
</div>

{{ Form::close() }}
@endsection

@section('styles')
    {!! HTML::style('ace/assets/css/datepicker.css') !!}
    {!! HTML::style('ace/assets/css/chosen.css') !!}

    <style>
        input[type=checkbox].ace.ace-switch.ace-switch-5+.lbl::before {
            content: "ДА\a0\a0\a0\a0\a0\a0\a0\a0\a0\a0\a0НЕТ";
        }
    </style>
@endsection

@section('scripts')

    @include('admin.partials.slug', ['input_name' => ['ru']])

    @include('admin.partials.ckeditor')

    @include('admin.partials.datepicker')

    @include('admin.partials.chosen')

@append
