@extends('admin.common.list',
    [
        'title'       =>  'Новости',
        'desc'        =>  'Список статей сайта',
        'model'       =>  'news',
        'fields'      =>  ['name' => 'Наименование', 'created_at' => 'Создан'],
        'data'        =>  $data,
        'no_visibility' => true,
    ]
)



