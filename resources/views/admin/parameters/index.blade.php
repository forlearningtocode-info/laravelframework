@extends('admin.common.list',
    [
        'title'       =>  'Параметры',
        'desc'        =>  'Список параметров для товара',
        'model'       =>  'parameters',
        'fields'      =>  ['name' => 'Наименование'],
        'data'        =>  $data,
        'no_visibility' => true,
    ]
)
