<ul class="nav nav-list">
    <li class="">
        <a href="{{ url('admin') }}">
            <i class="menu-icon fa fa-home"></i>
            <span class="menu-text"> Главная </span>
        </a>
    </li>
    <li>
        <a href="{{ url('admin/products') }}">
            <i class="menu-icon fa fa-home"></i>
            <span class="menu-text"> Проекты </span>
        </a>
    </li>
    <li class="">
        <a href="#" class="dropdown-toggle">
            <i class="menu-icon fa fa-cogs"></i>
            <span class="menu-text"> Администр.</span>
            <b class="arrow fa fa-angle-down"></b>
        </a>
        <b class="arrow"></b>
        <ul class="submenu">
            <li class="">
                <a href="{{ url('admin/users') }}">
                    <i class="menu-icon fa fa-users"></i>
                    Пользователи
                </a>
                <b class="arrow"></b>
            </li>
            <li>
                <a href="{{ url('admin/translations') }}">
                    <i class="menu-icon fa fa-globe"></i>
                    <span class="menu-text"> Переводы </span>
                </a>
            </li>
            {{--<li>
                <a href="{{ url('admin/lists') }}">
                    <i class="menu-icon fa fa-list-ol"></i>
                    <span class="menu-text"> Справочники </span>
                </a>
            </li>--}}
        </ul>
    </li>
    <li class="">
        <a href="{{ url('logout') }}">
            <i class="menu-icon fa fa-sign-out"></i>
            <span class="menu-text"> Выход </span>
        </a>
    </li>
</ul><!-- /.nav-list -->
