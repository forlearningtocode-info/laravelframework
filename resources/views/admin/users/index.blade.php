@extends('admin.body')
@section('title', 'Список пользователей')

@section('centerbox')
    <div class="page-header">
        <h1> Администрирование <small><i class="ace-icon fa fa-angle-double-right"></i> Пользователи </small> </h1>
    </div>

    <a class="btn btn-success" href="{{ route('admin.users.create') }}" title="Создать">
        <i class="ace-icon fa fa-plus-square-o bigger-120"></i>
        Создать
    </a>

    <div class="row">
        <div class="col-xs-12">
            <div class="clearfix"><div class="pull-right tableTools-container"></div></div>
            <div class="table-header"> Список пользователей </div>

            <table id="dynamic-table" class="table table-striped table-bordered table-hover dataTable">
                <thead>
                <tr>
                    <th align="center">ID</th>
                    <th align="center">ФИО</th>
                    <th align="center">Email</th>
                    <th align="center">Создан</th>
                    <th align="center"><i class="fa fa-cogs"></i></th>
                </tr>
                </thead>
                <tbody bgcolor="white"></tbody>
            </table>
        </div><!-- /.col-xs-12 -->
    </div><!-- /.row -->
@stop
@section('scripts')

    <script>
        var columns = [
            { data : 'id' },
            { data : 'name' },
            { data : 'email' },
            { data : 'created_at' },
            { data : 'actions', className : 'center' }
        ];
    </script>

    @include('admin.partials.datatable-init-ajax', ['ajax' => 'admin/get-users'])

@endsection