@extends('admin.body')
@section('title', 'Редактирование пользователя')
@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ url('admin/users') }}"> Пользователи</a>
            <small><i class="ace-icon fa fa-angle-double-right"></i> Редактирование</small> </h1>
    </div>

    @include('admin.partials.errors')

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/users', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/users/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('name', 'Имя', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('name', (isset($data->name) ? $data->name : old('name')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('email', 'Email', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('email', (isset($data->email) ? $data->email : old('email')), array('class' =>
                    'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="margin-top-35"></div>

            <div class="form-group">
                {{ Form::label('company', 'Название компании', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('company', (isset($data->params['company']) ? $data->params['company'] : old('company')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('fisc', 'Фискальный код', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('fisc', (isset($data->params['fisc']) ? $data->params['fisc'] : old('company')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('bank', 'Банк', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('bank', (isset($data->params['bank']) ? $data->params['bank'] : old('company')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('bank_account', 'Текущий счет', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('bank_account', (isset($data->params['bank_account']) ? $data->params['bank_account'] : old('bank_account')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('tva', 'НДС', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('tva', (isset($data->params['tva']) ? $data->params['tva'] : old('company')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('fax', 'Факс', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('fax', (isset($data->params['fax']) ? $data->params['fax'] : old('company')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('phone', 'Телефон', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('phone', (isset($data->phone) ? $data->phone : old('phone')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('address', 'Адрес', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('address', (isset($data->address) ? $data->address : old('address')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="margin-top-35"></div>

            <div class="form-group">
                {{ Form::label('password', 'Новый пароль', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('password', old('password'), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('comment', 'Комментарий', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::textarea('comment', (isset($data->comment) ? $data->comment : old
                    ('comment')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                {{ Form::label('rights', 'Роль', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('rights', ['0' => 'Пользователь', '1' => 'Администратор'], (isset($data->rights) ? $data->rights : old('rights')), ['class' => 'col-sm-9']) }}
                </div>
            </div>
            <div class="form-group">
                {{ Form::label('enabled', 'Активный', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::select('enabled', ['1' => 'Да', '0' => 'Нет'], (isset($data->enabled) ? $data->enabled : old('enabled')), ['class' => 'col-sm-9']) }}
                </div>
            </div>
        </div>
    </div><!-- row -->

    {{ Form::close() }}

@endsection

@section('scripts')

    @include('admin.partials.chosen')

@endsection