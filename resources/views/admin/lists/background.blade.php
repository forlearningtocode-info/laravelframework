@extends('admin.body')
@section('title', 'Справочники')


@section('centerbox')
    <div class="page-header">
        <h1> <a href="{{ URL::to('admin/lists') }}">Справочники</a><small><i class="ace-icon fa fa-angle-double-right"></i> Фоновая картинка</small> </h1>
    </div>

    @include('admin.partials.errors')

    @if(!isset($data))
        {{ Form::open(['url' => 'admin/lists', 'class' => 'form-horizontal']) }}
    @else
        {{ Form::open(['url' => 'admin/lists/' . $data->id, 'method' => 'put', 'class' => 'form-horizontal']) }}
    @endif

    <div class="form-actions">
        <div class="row center">
            <div class="col-sm-2">
                <button id="submit_button1" type="submit" class="btn  btn-success btn-block btn-responsive" ><i class="ace-icon fa fa-floppy-o  bigger-120"></i> Сохранить </button>
            </div>
            <div class="col-sm-4">
                <div class="profile-contact-info">
                    <div class="profile-links align-left">

                        @if (isset($data))
                            <div class="btn btn-link">
                                <i class="ace-icon fa fa- bigger-120 green"></i>
                                ID: {{ $data->id }}
                            </div>

                            <div class="btn btn-link">
                                <i class="ace-icon fa fa-calendar bigger-120 green"></i>
                                Изменен: {{ $data->updated_at }}
                            </div>
                        @endif
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.form-actions -->

    <div class="row">
        {{ Form::text('name[ru]', (isset($data->name) ? $data->name : ''), array('class' => 'col-sm-11 col-xs-12 name_ru hide')) }}

        <div class="col-sm-6 hide">
            <div class="form-group">
                {{ Form::label('slug[ru]', 'URL', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    {{ Form::text('slug[ru]', (isset($data->slug) ? $data->slug : old('slug')), array('class' => 'col-sm-11 col-xs-12')) }}
                </div>
            </div>

            <div class="form-group hide">
                <label for="mydate" class="col-sm-3 control-label no-padding-right"> Дата:</label>
                <div class="col-sm-5">
                    <div class="input-group">
                        <input type="date" name="date" class="form-control date-picker"
                               data-date-format="yyyy-mm-dd"
                               value="{{ (isset($data->created_at) ? date('Y-m-d', strtotime($data->created_at)) : old('date', date('Y-m-d'))) }}" />
                        <span class="input-group-addon">
                            <i class="fa fa-calendar bigger-110"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group hide">
                {{ Form::label('parent', 'Родитель', ['class'=>'col-sm-3 control-label no-padding-right']) }}
                <div class="col-sm-9">
                    @if(isset($parents))
                        @if(isset($parent_id))
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, $parent_id, ['class'=>'col-sm-11 col-xs-12']) }}
                        @else
                            {{ Form::select('parent_id', array("0" => "Нет родителя") + $parents, '', ['class'=>'col-sm-11 col-xs-12']) }}
                        @endif
                    @endif
                </div>
            </div>
        </div><!-- /.col-sm-6 -->
    </div><!-- /.row -->
    <hr>
    <div class="tabbable">
        <ul id="myTab4" class="nav nav-tabs padding-12 tab-color-blue background-blue">
            <li class="active">
                <a href="#photos" data-toggle="tab">Фото</a>
            </li>
        </ul>
    </div>

    <div class="tab-content">
        @include('admin.partials.photos', ['table' => 'lists', 'table_id' => isset($data->id) ? $data->id : 0, 'crop' => [1920, 1080], 'class' => 'active'] )

        @include('admin.partials.meta')

    </div>

    <div class="form-actions">
        {{ Form::submit('Сохранить', array('class' => 'btn btn-success')) }}
    </div>

    {{ Form::close() }}
@endsection

@section('styles')
@endsection

@section('scripts')
    <script>
        //
    </script>

    @include('admin.partials.slug', ['input_name' => ['ru']])

    @include('admin.partials.ckeditor')

    @include('admin.partials.slug',['input_name' => ['ru']])

@endsection
