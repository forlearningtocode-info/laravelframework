@extends('admin.body')
@section('title', 'Главная')

@section('centerbox')
<div class="page-header">
    <h1> Главная </h1>
</div>

<div class="alert alert-success">
    <button class="close" data-dismiss="alert"><i class="ace-icon fa fa-times"></i></button>
    Здравствуйте, @if (Auth::check()) {{ Auth::user()->name }} @endif! Управление сайтом в панели слева.
</div>

<div class="row main-page">
    <div class="col-xs-12">
        <h3 class="header smaller lighter green"></h3>

        <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/products') }}" class="btn btn-success btn-app radius-4">
                    <i class="ace-icon fa fa-home bigger-230"></i>
                    Проекты
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/lists') }}" class="btn btn-warning btn-app radius-4">
                    <i class="ace-icon fa fa-cubes bigger-230"></i>
                    Блоки
                </a>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12 center">
                <a href="{{ url('admin/translations') }}" class="btn btn-danger btn-app radius-4">
                    <i class="ace-icon fa fa-globe bigger-230"></i>
                    Переводы
                </a>
            </div>
        </div>
        <div class="space"></div>
    </div>
</div>
@stop
