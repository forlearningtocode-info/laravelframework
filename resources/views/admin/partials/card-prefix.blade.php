<script>
    var CARD_PREFIX = '{{ config('custom.default_card_prefix') }}';

    $(document).ready(function(){
        var value = $('.card-prefix').val();
        if (value === '') {
            $('.card-prefix').val(CARD_PREFIX);
        }
    });
</script>