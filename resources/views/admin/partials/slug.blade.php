<script>
    var FORM_ID = '{{ $form_id ?? ''}}';
    if (FORM_ID != "") FORM_ID = "#" + FORM_ID + " ";
    $(document).ready(function () {
        var a = {
            "Ё": "YO",
            "Й": "I",
            "Ц": "TS",
            "У": "U",
            "К": "K",
            "Е": "E",
            "Н": "N",
            "Г": "G",
            "Ш": "SH",
            "Щ": "SCH",
            "З": "Z",
            "Х": "H",
            "Ъ": "'",
            "ё": "yo",
            "й": "i",
            "ц": "ts",
            "у": "u",
            "к": "k",
            "е": "e",
            "н": "n",
            "г": "g",
            "ш": "sh",
            "щ": "sch",
            "з": "z",
            "х": "h",
            "ъ": "'",
            "Ф": "F",
            "Ы": "I",
            "В": "V",
            "А": "a",
            "П": "P",
            "Р": "R",
            "О": "O",
            "Л": "L",
            "Д": "D",
            "Ж": "ZH",
            "Э": "E",
            "ф": "f",
            "ы": "i",
            "в": "v",
            "а": "a",
            "п": "p",
            "р": "r",
            "о": "o",
            "л": "l",
            "д": "d",
            "ж": "zh",
            "э": "e",
            "Я": "Ya",
            "Ч": "CH",
            "С": "S",
            "М": "M",
            "И": "I",
            "Т": "T",
            "Ь": "'",
            "Б": "B",
            "Ю": "YU",
            "я": "ya",
            "ч": "ch",
            "с": "s",
            "м": "m",
            "и": "i",
            "т": "t",
            "ь": "'",
            "б": "b",
            "ю": "yu",
            "ă":"a",
            "î":"i",
            "â":"i",
            "ş":"s",
            "ț":"t",
        };

        @foreach($input_name as $input)
            transliterate_{{$input}} = function (Text) {
            if (Text) {
                return Text.split('').map(function (char) {
                    return a[char] || char;
                }).join("").toLowerCase().replace(/ +/g, '-');
            }

            return "";
        };
        $(FORM_ID + 'input[name="' + 'name[{{$input}}]' + '"]').keyup(function () {
            var a = $(this).val();
            var b = transliterate_{{$input}}(a);
            $(FORM_ID + 'input[name="' + 'slug[{{$input}}]' + '"]').val(b);
        });
        @endforeach
    });
</script>
