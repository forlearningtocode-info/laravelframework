@extends('body')

@section('aside')
    @include('aside-content')
@endsection

@section('content')
    <h1 class="title">
        <span>{{trans('common.registration_word')}}</span>
    </h1>
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal row" method="POST" action="{{ route('register') }}">
                            {{ csrf_field() }}


                            <div class="form-group сol-12 col-sm-6">
                                <label for="comp-name" class="control-label">{{trans('common.edit_company_name')}}</label>
                                <input id="comp-name" type="text" class="form-control" name="company" value="{{ old('company') }}" required>
                            </div>
                            <div class="form-group col-12 col-sm-6">
                                <label for="tax-code" class="control-label">{{trans('common.edit_tax')}}</label>
                                <input id="tax-code" type="text" class="form-control" name="fisc" value="{{ old('fisc') }}" required>
                            </div>
                            <div class="form-group col-12 col-sm-6">
                                <label for="tva" class="control-label">{{trans('common.edit_tva')}}</label>
                                <input id="tva" type="text" class="form-control" name="tva" value="{{ old('tva') }}">
                            </div>
                            <div class="form-group col-xs-12 col-sm-6">
                                <label for="phone" class="control-label">{{trans('common.edit_phone')}}</label>
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>
                            </div>

                            <div class="form-group col-12 col-sm-6{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">E-Mail</label>
                                <div class="">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <span>{{ $errors->first('email') }}</span>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-12 col-sm-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="control-label">{{trans('common.name')}}</label>
                                <div class="">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="help-block">
                                            <span>{{ $errors->first('name') }}</span>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-12 col-sm-6 {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="control-label">{{trans('common.password')}}</label>

                                <div class="">
                                    <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <span>{{ $errors->first('password') }}</span>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group col-12 col-sm-6">
                                <label for="password-confirm" class="control-label">{{trans('common.password_confirmation')}}</label>

                                <div class="">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group col-12">
                                <div class="">
                                    <button type="submit" class="btn btn-lg btn-orange" style="margin: 50px auto 0">
                                        {{trans('common.to_register')}}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection